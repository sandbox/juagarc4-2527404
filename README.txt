
PREPOPULATE FROM DB MODULE
==========================
By publicplan, based on the prepopulate module: https://drupal.org/project/prepopulate

Prepopulatefromdb is an attempt to solve the problem to fill a webform
with values saved in the database using a key parameter of the URL. 

http://www.example.com/node/add/blog?key=key-value

Refer to the USAGE.txt file or the online handbook at http://drupal.org/node/XXXX for more examples.

Please report any bugs or feature requests to the Prepopulatefromdb issue queue:
http://drupal.org/project/issues/prepopulatefromdb
