<?php

/**
 * @file
 * Functions to obtain and process data
 */

/**
 * @param $fieldValue string : The value to search
 * @param $form object . The form object
 * @return type Asssociative array with the data of the DB
 */
function webform_populate_get_submissions_by_field_value($fieldValue, $form) {

  $formId = $form['details']['nid']['#value'];
  $cid = webform_populate_get_component_id_by_name($formId, "kita_key");

  $submissions = db_select('webform_submitted_data', 'wsd')
    ->condition('wsd.data', $fieldValue, '=')
    ->condition('wsd.cid', $cid, '=')
    ->fields('wsd', array('nid', 'sid'))
    ->execute()
    ->fetchAssoc();
  return $submissions;
}

/**
 *
 * @param type $nid : Node ID (Webform)
 * @param type $sid : Submission ID
 * @return array $results Data corresponding to $nid and $sid.
 */
function webfom_populate_get_submission_data($nid, $sid) {
  $query = db_select('webform_component', 'wc');
  $query->innerJoin('webform_submitted_data', 'wsd', 'wsd.cid = wc.cid');
  $query
    ->fields('wc', array('form_key'))
    ->fields('wsd', array('data', 'nid', 'sid'))
    ->condition('wsd.sid', $sid)
    ->condition('wsd.nid', $nid);
  $results = $query
    ->execute()
    ->fetchAll();

  return $results;
}

/**
 * Return the name of a component (field)
 * @param type $nid
 * @param type $cid
 * @return string field name
 */
function get_component_name($nid, $cid) {
  $field = db_select('webform_component', 'wc')
    ->condition('wc.nid', $nid, '=')
    ->condition('wc.cid', $cid, '=')
    ->fields('wc', array('form_key'))
    ->execute()
    ->fetchAssoc();

  return $field['form_key'];
}

/**
 * Return the component id of the webform
 * @param string $nid
 * @param string  $name
 * @return integer id of the component
 */
function webform_populate_get_component_id_by_name($nid, $name) {


  $fields = db_select('webform_component', 'wc')
    ->condition('wc.nid', $nid, '=')
    ->condition('wc.form_key', $name, '=')
    ->fields('wc', array('cid'))
    ->execute()
    ->fetchAssoc();

  return $fields['cid'];
}

/**
 * This function returns tne number of submissions sent.
 * Due to a submission has data, we don't know if a submission
 * is original or updated.
 * Therefore we need a field that it must be filled for a user.
 * If the field is filled, it will be an update.
 *
 * @param string $fieldName
 * @param mixed  $fieldValue
 * @param mixed  $form
 *
 * @return int Number of records
 *
 */
function webform_populate_count_submissions_by_field($fieldName, $fieldValue, &$form) {

  $formId = $form['details']['nid']['#value'];

  $field = db_select('webform_component', 'wc')
    ->condition('wc.nid', $formId, '=')
    ->condition('wc.form_key', $fieldName, '=')
    ->fields('wc', array('cid'))
    ->execute()
    ->fetchAssoc();

  $fieldId = $field['cid'];

  $submissions = db_select('webform_submitted_data', 'wsd')
    ->condition('wsd.nid', $formId, '=')
    ->condition('wsd.cid', $fieldId, '=')
    ->condition('wsd.data', $fieldValue, '=')
    ->fields('wsd', array('nid', 'data'))
    ->execute();
  $submissionCount = $submissions->rowCount();

  return $submissionCount;
}

/**
 * @param string $nid
 * @param string  $sid
 * @return void
 */
function webform_populate_delete_submission_data($nid, $sid) {

  $fields = db_select('webform_component', 'wc')
    ->condition('wc.nid', $nid, '=')
    ->fields('wc', array('cid', 'form_key'))
    ->execute()
    ->fetchAllKeyed(0, 1);

  $fieldsToDelete = array('anrede', 'vorname', 'name', 'email', 'telefon', 'veranstaltung', 'fachvortrag', 'vegetarisch');
  foreach ($fields as $index => $fieldName) {
    if (in_array($fieldName, $fieldsToDelete)) {
      $num_deleted = db_delete('webform_submitted_data')
        ->condition('sid', $sid)
        ->condition('cid', $index)
        ->execute();
    }
  }
}

/**
 * This function calculates the free places by city and returns the result message.
 * @param type $form
 * @return array $errors
 */
function webform_populate_test_free_places(&$form) {

  // error message and type to show
  $errors = array();

  if (isset($form["submitted"]["veranstaltung"]["#value"])) {
    $stadtValue = $form['submitted']['veranstaltung']['#value'];
  } else {
    $stadtValue = $form['submitted']['veranstaltung']['#default_value'];
  }

  $stadDescription = $form['submitted']['veranstaltung']["#options"][$stadtValue];
  $submissionPerOrt = webform_populate_count_submissions_by_field('veranstaltung', $stadtValue, $form);
  $factor = 50;

  switch ($stadtValue) {
    case 1:
      $originalPlaces = 1550;
      $places = $originalPlaces - $factor;
      break;
    case 2:
      $originalPlaces = 1200;
      $places = $originalPlaces - $factor;
      break;
    case 3:
      $originalPlaces = 600;
      $places = $originalPlaces - $factor;
      break;
    case 4:
      $originalPlaces = 1000;
      $places = $originalPlaces - $factor;
      break;
    case 5:
      $originalPlaces = 1200;
      $places = $originalPlaces - $factor;
      break;
    case 6:
      $originalPlaces = 1200;
      $places = $originalPlaces - $factor;
    case 7:
      $originalPlaces = 1000;
      $places = $originalPlaces - $factor;
      break;
    case 8:
      $originalPlaces = 700;
      $places = $originalPlaces - $factor;
      break;
    case 9:
      $originalPlaces = 1000;
      $places = $originalPlaces - $factor;
      break;
    default:
      $places = 100000;
  }
  $percent = $places * 0.10;
  $freePlaces = $places - $submissionPerOrt;
  $_SESSION["places_full"] = false;

  if ($freePlaces <= 0) {
    if ($_SESSION["operation"] == "insert") {
      $msg = t("Die Veranstaltung am $stadDescription ist bereits ausgebucht. <br/>Bitte versuchen Sie sich für eine andere Veranstaltung anzumelden.");
      $errors["type"] = "error";
      unset($form['actions']['submit']);
    } else {
      $msg = t("Die Veranstaltung am $stadDescription ist bereits ausgebucht. <br/>Sie können aber Ihre Anmeldung anpassen oder stornieren.");
      $errors["type"] = "warning";
    }
    $_SESSION["places_full"] = true;
    $errors["msg"] = $msg;
  } elseif ($freePlaces <= $percent) {

    $msg = t("Es sind nur noch wenige Plätze für die Veranstaltung am $stadDescription frei.");
    $errors["msg"] = $msg;
    $errors["type"] = "warning";
  }
  return $errors;
}

/**
 * This functions test if we are in time to register or not
 * @param type $daysToEvent
 * @return array $errors. Array with fields msg and type(error, warning)
 */
function webform_populate_test_time(&$form) {
  $errors = array();

  if (isset($form["submitted"]["veranstaltung"]["#value"])) {
    $selectedEvent = $form["submitted"]["veranstaltung"]["#value"];
  } else {
    $selectedEvent = $form["submitted"]["veranstaltung"]["#default_value"];
  }
  $eventOption = $form["submitted"]["veranstaltung"]["#options"][$selectedEvent];
  $eventParts = explode("|", $eventOption);
  $eventDate = $eventParts[0];  // Format is d.m.Y

  $daysToEvent = webform_populate_get_days_between_dates($eventDate);
  $stadDescription = $form['submitted']['veranstaltung']["#options"][$selectedEvent];

  if (($daysToEvent < 14) and ( $daysToEvent >= 0)) {
    if (($selectedEvent != 3) and ( $selectedEvent != 5) and ( $selectedEvent != 6) and ( $selectedEvent != 8) and ( $selectedEvent != 9)) {
      $msg = t("Leider ist eine Anmeldung, Änderung oder Stornierung zur Veranstaltung am $stadDescription zu diesem Zeitpunkt nicht mehr möglich. <br/>"
        . "Vielen Dank für Ihr Verständnis.");

      $errors["msg"] = $msg;
      $errors["type"] = "error";
      unset($form['actions']['submit']);
      unset($form['actions']['delete']);
    }
  } elseif ($daysToEvent < 0) {
    $msg = t("Leider ist eine Anmeldung, Änderung oder Stornierung zur Veranstaltung am $stadDescription zu diesem Zeitpunkt nicht mehr möglich. <br/>"
      . "Vielen Dank für Ihr Verständnis.");

    $errors["msg"] = $msg;
    $errors["type"] = "error";
    unset($form['actions']['submit']);
    unset($form['actions']['delete']);
  }
  return $errors;
}

/**
 * This function composes our Email with subject an get the text of the form fields
 *
 * @param type $node
 * @param type $sid
 * @return type
 *
 */
function webform_populate_send_email(&$node, $sid = "") {
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  module_load_include('inc', 'webform', 'includes/webform.emails');

  $nid = $node->nid;
  if (isset($_GET['sid'])) {
    $sid = filter_var($_GET['sid'], FILTER_SANITIZE_NUMBER_INT);
  }
  $submission = webform_get_submission($nid, $sid);

  $operation = $_SESSION['operation'];

  switch ($operation) {
    case "insert":
      $body = field_get_items('node', $node, 'field_e_mail_anmeldung');
      $node->webform['emails'][1]['subject'] = "Fachinformationstage Sprache: Anmeldebestätigung";
      $node->webform['emails'][1]['template'] = $body[0]['value'];
      break;
    case "update":

      $body = field_get_items('node', $node, 'field_e_mail_update');
      $node->webform['emails'][1]['subject'] = "Fachinformationstage Sprache: Änderungsbestätigung";
      $node->webform['emails'][1]['template'] = $body[0]['value'];
      break;
    case "delete":
      $body = field_get_items('node', $node, 'field_e_mail_stornierung');
      $node->webform['emails'][1]['subject'] = "Fachinformationstage Sprache: Stornierungsbestätigung";
      $node->webform['emails'][1]['template'] = $body[0]['value'];
      break;
  }

  $numEmails = webform_submission_send_mail($node, $submission);
  return $numEmails;
}

/**
 * This functions aborts the sending of emails rom the standard function
 * of webforms. To give the correct functionality, the function tests if subject is blank.
 * this method is not perfect but its easy to manage in backend.
 * So we can use our functions and send the emails, when we want.
 * @param boolean $message
 */
function webform_populate_mail_alter(&$message) {
  //You can do your conditional check here and depending on that
  //you can set the value of $message['send'] to false to stop the mail from being sent.
  //place the line below inside conditional loop, or else it'll stop every mail from being sent ;-)
  //If our subject is blank, we stops sending the standard email from webforms
  if ($message['subject'] == "") {
    $message['send'] = FALSE;
  }
}

/**
 *  This function calculates the difference in Days between current date and the event date.
 *  @param string $dateTo: The end date to calculate the difference.
 *  @param string $dateFrom: The start date. If not provided, "now" will be used.
 *  @return int Number of Days between dates
 */
function webform_populate_get_days_between_dates($dateTo, $dateFrom = "now") {

//Using approach PHP 5.3 > , this is by far the most accurate way of calculating the difference.
  $d1 = new DateTime($dateTo);
  $d2 = new DateTime($dateFrom);

  if ($d2 > $d1) {
    $days = -1;
  } else {
    $days = $d2->diff($d1)->format("%a"); // Return always a positive value (days)
  }

  return $days;
}

/**
 * Show an error
 * @param string $errorText: The text to show
 * @param string $type: The type of error.
 * error=> Abort the execution.
 * warning => Drupal warning.
 * Default: warning
 * @return none
 */
function webform_populate_show_errors($errorText, $type = "warning") {
  if (!empty($errorText)) {
    switch ($type) {
      case "error":
        form_set_error('', $errorText);
        break;
      case "warning":
        drupal_set_message($errorText, 'warning');
        break;
    }
  }
}
